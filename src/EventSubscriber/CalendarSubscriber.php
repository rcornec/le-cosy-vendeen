<?php

namespace App\EventSubscriber;

use App\Entity\BookingStatus;
use App\Entity\User;
use App\Model\BookingStatusModel;
use App\Repository\BookingRepository;
use CalendarBundle\CalendarEvents;
use CalendarBundle\Entity\Event;
use CalendarBundle\Event\CalendarEvent;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\NoReturn;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CalendarSubscriber implements EventSubscriberInterface
{
    /**
     * CalendarSubscriber constructor.
     */
    public function __construct(
        public BookingRepository $bookingRepository,
        private readonly TokenStorageInterface $tokenStorage
    ) {
    }

    #[ArrayShape([CalendarEvents::SET_DATA => 'string'])]
    public static function getSubscribedEvents(): array
    {
        return [
            CalendarEvents::SET_DATA => 'onCalendarSetData',
        ];
    }

    /**
     * @throws \Exception
     */
    #[NoReturn]
    public function onCalendarSetData(CalendarEvent $calendar): void
    {
        $start = $calendar->getStart();
        $end = $calendar->getEnd();

        // You may want to make a custom query from your database to fill the calendar

        $results = $this->bookingRepository->findAllBetweenTwoDates($start, $end);

        [$currentUser, $isAdmin] = $this->getCurrentUser();

        foreach ($results as $result) {
            $color = $this->determineColor($result->getStatus(), $result->getUser(), $currentUser, $isAdmin);
            $calendar->addEvent(new Event(
                $isAdmin ? $this->determineTitle($result->getUser()) : '',
                $result->getDateStart(),
                $result->getDateEnd()->add(new \DateInterval('P1D')),
                null,
                ['allDay' => true, 'color' => $color]
            ));
        }
    }

    private function determineColor(
        BookingStatus $status,
        User $bookingUser,
        User $currentUser,
        bool $isAdmin
    ): string {
        if ($bookingUser === $currentUser || $isAdmin) {
            return constant('\App\Model\BookingStatusModel::'.$status->getName().'_COLOR');
        }

        return BookingStatusModel::DEFAULT_COLOR;
    }

    private function determineTitle(User $user): string
    {
        return sprintf(
            '%s - %s - %s',
            $user->getFullName(),
            $user->getEmail(),
            $user->getPhoneNumber()
        );
    }

    private function getCurrentUser(): array
    {
        if (null !== $this->tokenStorage->getToken() && null !== $this->tokenStorage->getToken()->getUser()) {
            $user = $this->tokenStorage->getToken()->getUser();

            $isAdmin = in_array('ROLE_ADMIN', $user->getRoles(), true)
                || in_array('ROLE_SUPER_ADMIN', $user->getRoles(), true);

            return [$user, $isAdmin];
        }

        return [null, false];
    }
}
