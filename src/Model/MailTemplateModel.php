<?php

namespace App\Model;

class MailTemplateModel
{
    public const string BOOKING_ASK_USER_TEMPLATE = 'email/booking_ask_user.html.twig';
    public const string BOOKING_ASK_ADMIN_TEMPLATE = 'email/booking_ask_admin.html.twig';
    public const string BOOKING_VALIDATED_TEMPLATE = 'email/booking_change_status.html.twig';
    public const string BOOKING_REFUSED_TEMPLATE = 'email/booking_change_status.html.twig';
    public const string BOOKING_CANCELED_TEMPLATE = 'email/booking_change_status.html.twig';

    public const string BOOKING_ASK_USER_SUBJECT = 'Le Cosy Vendéen - Réservation';
    public const string BOOKING_ASK_ADMIN_SUBJECT = 'Le Cosy Vendéen - Demande de réservation';
    public const string BOOKING_VALIDATED_SUBJECT = 'Le Cosy Vendéen - Votre Réservation a été validée';
    public const string BOOKING_REFUSED_SUBJECT = 'Le Cosy Vendéen - Votre Réservation a été refusée';
    public const string BOOKING_CANCELED_SUBJECT = 'Le Cosy Vendéen - Votre Réservation a été annulée';
}
