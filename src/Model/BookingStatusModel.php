<?php

namespace App\Model;

class BookingStatusModel
{
    public const string CREATED = 'CREATED';
    public const string VALIDATED = 'VALIDATED';
    public const string REFUSED = 'REFUSED';
    public const string CANCELED = 'CANCELED';

    public const string CREATED_COLOR = '#ffc107';
    public const string VALIDATED_COLOR = '#198754';
    public const string REFUSED_COLOR = '#dc3545';
    public const string DEFAULT_COLOR = '#0d6efd';
    public const string CANCELED_COLOR = '#0d6efd';

    public const string CREATED_BTCOLOR = 'warning';
    public const string VALIDATED_BTCOLOR = 'success';
    public const string REFUSED_BTCOLOR = 'danger';
    public const string PAST_BTCOLOR = 'secondary';

    public const string VALIDATED_FR = 'validée';
    public const string REFUSED_FR = 'refusée';
    public const string CANCELED_FR = 'annulée';

    public static function exists(string $status): bool
    {
        return in_array($status, [
            self::CREATED,
            self::VALIDATED,
            self::REFUSED,
            self::CANCELED,
        ]);
    }
}
