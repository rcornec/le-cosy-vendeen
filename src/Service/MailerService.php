<?php

namespace App\Service;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

readonly class MailerService
{
    /**
     * MailerService constructor.
     */
    public function __construct(private MailerInterface $mailer)
    {
    }

    /**
     * @param ...$to
     *
     * @throws TransportExceptionInterface
     */
    public function sendMail(string $subject, string $template, array $variables, ...$to): void
    {
        $email = (new TemplatedEmail())
            ->to(...$to)
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($variables);

        $this->mailer->send($email);
    }
}
