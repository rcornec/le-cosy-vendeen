<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\User;
use App\Form\BookingFormType;
use App\Form\CancelBookingFormType;
use App\Form\EditProfileFormType;
use App\Model\BookingStatusModel;
use App\Model\MailTemplateModel;
use App\Repository\BookingRepository;
use App\Repository\BookingStatusRepository;
use App\Repository\UserRepository;
use App\Service\MailerService;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\UX\Turbo\TurboBundle;

class IndexController extends AbstractController
{
    public function __construct(
        private readonly BookingStatusRepository $bookingStatusRepository,
        private readonly BookingRepository $bookingRepository,
        private readonly MailerService $mailerService,
        private readonly UserRepository $userRepository
    ) {
    }

    #[Route('/', name: 'home')]
    public function home(): Response
    {
        return $this->render('home.html.twig');
    }

    #[Route('/welcome', name: 'welcome', methods: ['GET', 'HEAD'])]
    public function welcome(): Response
    {
        return $this->render('pages/welcome.html.twig');
    }

    #[Route('/presentation', name: 'presentation', methods: ['GET', 'HEAD'])]
    public function presentation(): Response
    {
        $finder = new Finder();
        $finder->files()->in(__DIR__.'/../../assets/images');

        $imageNames = [];

        if ($finder->hasResults()) {
            foreach ($finder as $image) {
                if (in_array($image->getExtension(), ['JPG', 'jpg', 'png'])) {
                    $imageNames[] = $image->getFilename();
                }
            }
        }

        return $this->render('pages/presentation.html.twig', ['imageNames' => $imageNames]);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws EntityNotFoundException
     */
    #[Route('/booking', name: 'booking')]
    public function booking(Request $request, ManagerRegistry $doctrine): Response
    {
        $booking = new Booking();
        $form = $this->createForm(BookingFormType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($booking->getDateStart() < new \DateTime()) {
                $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
                return $this->render(
                    'stream/alert.stream.html.twig',
                    [
                        'type' => 'danger',
                        'message' => 'Vous ne pouvez pas demander une réservation qui commence avant aujourd\'hui.',
                    ]
                );
            }

            $entityManager = $doctrine->getManager();

            $status = $this->bookingStatusRepository->findOneBy(['name' => BookingStatusModel::CREATED]);
            $user = $this->getUser();

            if (null === $status) {
                throw new EntityNotFoundException('The CREATED status is not found');
            }

            $booking->setStatus($status);

            if (!$user instanceof User) {
                throw new EntityNotFoundException('The User is not found');
            }

            $booking->setUser($user);

            $entityManager->persist($booking);
            $entityManager->flush();

            $this->mailerService->sendMail(
                MailTemplateModel::BOOKING_ASK_USER_SUBJECT,
                MailTemplateModel::BOOKING_ASK_USER_TEMPLATE,
                ['booking' => $booking],
                $user->getEmail()
            );

            $adminEmail = $this->userRepository->findAdminEmail();

            $this->mailerService->sendMail(
                MailTemplateModel::BOOKING_ASK_ADMIN_SUBJECT,
                MailTemplateModel::BOOKING_ASK_ADMIN_TEMPLATE,
                ['booking' => $booking],
                ...$adminEmail
            );
            $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
            return $this->render(
                'stream/alert.stream.html.twig',
                [
                    'type' => 'success',
                    'message' => 'Votre demande a bien été prise en compte, vous allez recevoir un mail pour récapituler votre réservation, puis un autre lors de sa validation.',
                ]
            );
        }

        return $this->render('pages/booking.html.twig', [
            'bookingForm' => $form->createView(),
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/my-bookings', name: 'my-bookings')]
    public function myBookings(Request $request, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(CancelBookingFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $booking = $this->bookingRepository->findOneBy(['id' => $form->getData()->getId()]);

                if (null === $booking) {
                    throw new \Exception(sprintf('Booking with id [%d] is not found', $form->getData()->getId()));
                }

                if ($booking->getUser() !== $this->getUser()) {
                    $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
                    return $this->render(
                        'stream/alert.stream.html.twig',
                        [
                            'bookingId' => $form->getData()->getId(),
                            'type' => 'danger',
                            'message' => 'Vous n\'êtes pas autorisé à effectuer cette action',
                        ]
                    );
                }

                $bookingStatus = $this->bookingStatusRepository->findOneBy(['name' => BookingStatusModel::CANCELED]);

                if (null === $bookingStatus) {
                    throw new \Exception(sprintf('Booking Status with name [%s] is not found', BookingStatusModel::CANCELED));
                }

                $booking->setStatus($bookingStatus);

                $entityManager = $doctrine->getManager();
                $entityManager->persist($booking);
                $entityManager->flush();

                $this->mailerService->sendMail(
                    MailTemplateModel::BOOKING_CANCELED_SUBJECT,
                    MailTemplateModel::BOOKING_CANCELED_TEMPLATE,
                    [
                        'booking' => $booking,
                        'status' => constant('\App\Model\BookingStatusModel::CANCELED_FR'),
                        'color' => constant('\App\Model\BookingStatusModel::CANCELED_COLOR'),
                    ],
                    $booking->getUser()->getEmail()
                );

                $adminEmail = $this->userRepository->findAdminEmail();

                $this->mailerService->sendMail(
                    MailTemplateModel::BOOKING_CANCELED_SUBJECT,
                    MailTemplateModel::BOOKING_CANCELED_TEMPLATE,
                    [
                        'booking' => $booking,
                        'status' => constant('\App\Model\BookingStatusModel::CANCELED_FR'),
                        'color' => constant('\App\Model\BookingStatusModel::CANCELED_COLOR'),
                    ],
                    ...$adminEmail
                );

                $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
                return $this->render(
                    'stream/remove_card.stream.html.twig',
                    [
                        'bookingId' => $form->getData()->getId(),
                        'type' => 'success',
                        'message' => 'Votre réservation a été annulée.',
                    ]
                );
            } catch (TransportExceptionInterface) {
                $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
                return $this->render(
                    'stream/alert.stream.html.twig',
                    [
                        'type' => 'danger',
                        'message' => 'Nous avons un problème avec l\'envoie de l\'email, mais votre réservation a bien été annulée.',
                    ]
                );
            } catch (\Exception $exception) {
                $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
                return $this->render(
                    'stream/alert.stream.html.twig',
                    [
                        'type' => 'danger',
                        'message' => sprintf('Nous n\'avons pas réussi à supprimer votre réservation (%s)', $exception->getMessage()),
                    ]
                );
            }
        }

        $bookingList = $this->bookingRepository->findByUser($this->getUser());

        return $this->render('pages/my_bookings.html.twig', [
            'bookingList' => $bookingList,
            'formDelete' => $form->createView(),
        ]);
    }

    #[Route('/about', name: 'about', methods: ['GET', 'HEAD'])]
    public function about(): Response
    {
        return $this->render('pages/about.html.twig');
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/edit-profile', name: 'edit-profile')]
    public function editProfile(Request $request, ManagerRegistry $doctrine): Response
    {
        if (!$this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('pages/not_connected.html.twig');
        }

        $user = $this->getUser();
        $form = $this->createForm(EditProfileFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $doctrine->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
            return $this->render(
                'stream/alert.stream.html.twig',
                [
                    'type' => 'success',
                    'message' => 'Votre profil a bien été mis à jour.',
                ]
            );
        }

        return $this->render('pages/edit_profile.html.twig', ['form' => $form->createView()]);
    }
}
