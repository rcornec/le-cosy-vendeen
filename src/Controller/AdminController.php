<?php

namespace App\Controller;

use App\Form\AdminUpdateBookingFormType;
use App\Model\BookingStatusModel;
use App\Repository\BookingRepository;
use App\Repository\BookingStatusRepository;
use App\Service\MailerService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\UX\Turbo\TurboBundle;

#[Route('/admin', name: 'admin_')]
#[IsGranted(new Expression(
    'is_granted("ROLE_ADMIN") or is_granted("ROLE_SUPER_ADMIN")'
))]
class AdminController extends AbstractController
{
    public function __construct(
        private readonly BookingStatusRepository $bookingStatusRepository,
        private readonly BookingRepository $bookingRepository,
        private readonly MailerService $mailerService,
    ) {
    }

    #[Route('/booking', name: 'booking')]
    public function booking(Request $request, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(AdminUpdateBookingFormType::class);
        $form->handleRequest($request);
        $bookingList = $this->bookingRepository->findByNotPastOrCanceled();

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $booking = $this->bookingRepository->findOneBy(['id' => $form->getData()->getId()]);
                $entityManager = $doctrine->getManager();

                $newStatus = constant('\App\Model\BookingStatusModel::'.$form->getData()->getStatus()->getName());
                $bookingStatus = $this->bookingStatusRepository->findOneBy(['name' => $newStatus]);

                if (
                    null === $booking
                    || null === $bookingStatus
                    || !BookingStatusModel::exists($form->getData()->getStatus()->getName())
                ) {
                    throw new \Exception();
                }

                $booking->setStatus($bookingStatus);

                $entityManager->persist($booking);
                $entityManager->flush();

                $this->mailerService->sendMail(
                    constant('\App\Model\MailTemplateModel::BOOKING_'.$newStatus.'_SUBJECT'),
                    constant('\App\Model\MailTemplateModel::BOOKING_'.$newStatus.'_TEMPLATE'),
                    [
                        'booking' => $booking,
                        'status' => constant('\App\Model\BookingStatusModel::'.$newStatus.'_FR'),
                        'color' => constant('\App\Model\BookingStatusModel::'.$newStatus.'_COLOR'),
                    ],
                    $booking->getUser()->getEmail()
                );

                $request->setRequestFormat(TurboBundle::STREAM_FORMAT);

                return $this->render(
                    'stream/update_card.stream.html.twig',
                    [
                        'booking' => $booking,
                        'type' => 'success',
                        'message' => sprintf(
                            'La réservation du %s a bien été %s',
                            $booking->getDateStart()->format('d/m/Y'),
                            constant('\App\Model\BookingStatusModel::'.$booking->getStatus()->getName().'_FR')
                        ),
                    ]
                );
            } catch (TransportExceptionInterface) {
                $request->setRequestFormat(TurboBundle::STREAM_FORMAT);

                return $this->render(
                    'stream/alert.stream.html.twig',
                    [
                        'type' => 'danger',
                        'message' => 'Nous avons un problème avec l\'envoie de l\'email, mais votre réservation a bien été prise en compte.',
                    ]
                );
            } catch (\Exception $exception) {
                $request->setRequestFormat(TurboBundle::STREAM_FORMAT);

                return $this->render(
                    'stream/alert.stream.html.twig',
                    [
                        'type' => 'danger',
                        'message' => sprintf('Nous n\'avons pas réussi à mettre à jour la réservation (%s)', $exception->getMessage()),
                    ]
                );
            }
        }

        return $this->render('admin/booking.html.twig', [
            'formUpdate' => $form->createView(),
            'bookingList' => $bookingList,
        ]);
    }
}
