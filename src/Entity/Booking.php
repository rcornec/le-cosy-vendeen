<?php

namespace App\Entity;

use App\Repository\BookingRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: BookingRepository::class)]
class Booking
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $dateStart;

    #[ORM\Column(type: 'datetime')]
    #[Assert\GreaterThanOrEqual(propertyPath: 'dateStart')]
    private \DateTimeInterface $dateEnd;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'bookings')]
    #[ORM\JoinColumn(nullable: false)]
    private User $user;

    #[ORM\ManyToOne(targetEntity: BookingStatus::class, inversedBy: 'bookings')]
    #[ORM\JoinColumn(nullable: false)]
    private BookingStatus $status;

    #[ORM\Column(type: 'integer', length: 2)]
    private int $numberOfPeople;

    public function getId(): int
    {
        return $this->id;
    }

    public function getDateStart(): \DateTimeInterface
    {
        return $this->dateStart;
    }

    public function setDateStart(\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd(): \DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getStatus(): BookingStatus
    {
        return $this->status;
    }

    public function setStatus(BookingStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNumberOfPeople(): int
    {
        return $this->numberOfPeople;
    }

    /**
     * @return $this
     */
    public function setNumberOfPeople(int $numberOfPeople): self
    {
        $this->numberOfPeople = $numberOfPeople;

        return $this;
    }
}
