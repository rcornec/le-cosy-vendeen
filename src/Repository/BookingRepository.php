<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Model\BookingStatusModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    /**
     * @return Booking[] Returns an array of Booking objects
     */
    public function findAllBetweenTwoDates(\DateTimeInterface $dateStart, \DateTimeInterface $dateEnd): array
    {
        return $this->createQueryBuilder('b')
            ->select('b', 's')
            ->join('b.status', 's')
            ->where('s.name <> :status')
            ->andWhere('(b.dateStart >= :dateStart OR b.dateEnd <= :dateEnd)')
            ->setParameter('status', BookingStatusModel::CANCELED)
            ->setParameter('dateStart', $dateStart)
            ->setParameter('dateEnd', $dateEnd)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByUser(UserInterface $user)
    {
        return $this->createQueryBuilder('b')
            ->join('b.status', 's')
            ->where('b.user = :user')
            ->andWhere('s.name <> :status')
            ->orderBy('b.dateStart', 'DESC')
            ->setParameter('user', $user)
            ->setParameter('status', BookingStatusModel::CANCELED)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByNotPastOrCanceled()
    {
        return $this->createQueryBuilder('b')
            ->join('b.status', 's')
            ->where('b.dateEnd >= :today')
            ->andWhere('s.name <> :status')
            ->orderBy('b.dateStart', 'DESC')
            ->setParameter('status', [BookingStatusModel::CANCELED])
            ->setParameter('today', new \DateTime('tomorrow'))
            ->getQuery()
            ->getResult()
        ;
    }
}
