<?php

namespace App\Repository;

use App\Entity\BookingStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BookingStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingStatus[]    findAll()
 * @method BookingStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BookingStatus::class);
    }
}
