import { Controller } from 'stimulus';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="navbar" attribute will cause
 * this controller to be executed. The name "navbar" comes from the filename:
 * navbar_controller.js -> "navbar"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    static targets = ["loader"]

    connect() {
        let loaderTarget = this.loaderTarget
        document.addEventListener("turbo:submit-start", function (e) {
            loaderTarget.classList.remove('visually-hidden')
        })
        document.addEventListener("turbo:before-fetch-request", function (e) {
            loaderTarget.classList.remove('visually-hidden')
        })
        document.addEventListener("turbo:before-fetch-response", function (e) {
            loaderTarget.classList.add('visually-hidden')
        })
    }
}
