import {Controller} from 'stimulus';
import {Calendar} from '@fullcalendar/core'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction';
import momentPlugin from '@fullcalendar/moment';
import {Modal} from 'bootstrap';

export default class extends Controller {
    static targets = ["output", "alert", "modal", "dateStart", "dateEnd"]
    static values = { user: Boolean }
    modal = null;
    calendar = null;

    connect() {
        let modalTarget = this.modalTarget;
        let dateStartTarget = this.dateStartTarget;
        let dateEndTarget = this.dateEndTarget;
        let userValue = this.userValue;

        this.modal = new Modal(modalTarget);
        let modal = this.modal

        this.calendar = new Calendar(this.outputTarget, {
            plugins: [dayGridPlugin, interactionPlugin, momentPlugin ],
            defaultView: 'dayGridMonth',
            editable: true,
            locale: 'fr',
            selectable: true,
            unselectAuto: true,
            allDay: true,
            firstDay: 1,
            buttonText: {
                today: 'Aujourd\'hui',
            },
            eventSources: [
                {
                    url: "/fc-load-events",
                    method: "POST",
                    extraParams: {
                        filters: JSON.stringify({})
                    },
                    failure: () => {
                        console.log("There was an error while fetching FullCalendar!");
                    },
                },
            ],
            select: function (selectionInfo) {
                if (userValue) {
                    let dateEnd = new Date(selectionInfo.end.getTime());
                    dateEnd.setDate(selectionInfo.end.getDate() - 1);

                    let dateEndStr = transformDateToHtmlDate(dateEnd) ;

                    dateStartTarget.value = selectionInfo.startStr
                    dateEndTarget.value = dateEndStr
                    modal.show();
                }
            }
        })
        this.calendar.render()

        function transformDateToHtmlDate (date) {
            let day = ("0" + date.getDate()).slice(-2);
            let month = ("0" + (date.getMonth() + 1)).slice(-2);
            return date.getFullYear() + "-" + (month) + "-" + (day);
        }
    }

    hideModal(event) {
        this.modal.hide();
        setTimeout(() => {  this.calendar.refetchEvents() }, 2000);
    }
}
