import { Controller } from 'stimulus';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="navbar" attribute will cause
 * this controller to be executed. The name "navbar" comes from the filename:
 * navbar_controller.js -> "navbar"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    static targets = ["pages"]
    changeActive(event) {
        this.pagesTargets.forEach( (elem) => { elem.classList.remove('active') } )
        event.target.classList.add('active')
    }
}
