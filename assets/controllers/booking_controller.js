import {Controller} from 'stimulus';
import {Calendar} from '@fullcalendar/core'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction';
import momentPlugin from '@fullcalendar/moment';
import {Modal} from 'bootstrap';

export default class extends Controller {
    static targets = ["modal", "delete"]
    modal = null;
    show (event) {
        console.log(event.target.attributes)
        let idBooking = event.target.attributes['booking-id'].value

        this.deleteTarget.value = idBooking

        this.modal = new Modal(this.modalTarget);
        this.modal.show();
    }

    hide(event) {
        this.modal.hide()
    }
}
