import {Controller} from 'stimulus';
import {Calendar} from '@fullcalendar/core'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction';
import momentPlugin from '@fullcalendar/moment';
import {Modal} from 'bootstrap';

export default class extends Controller {
    static targets = ["modal", "updateId", "updateStatus", "status"]
    modal = null;
    validate (event) {
        let idBooking = event.target.attributes['booking-id'].value

        this.updateIdTarget.value = idBooking
        this.updateStatusTarget.value = 'VALIDATED'
        this.statusTarget.innerHTML = 'valider'

        this.modal = new Modal(this.modalTarget);
        this.modal.show();
    }

    refuse (event) {
        let idBooking = event.target.attributes['booking-id'].value

        this.updateIdTarget.value = idBooking
        this.updateStatusTarget.value = 'REFUSED'
        this.statusTarget.innerHTML = 'refuser'

        this.modal = new Modal(this.modalTarget);
        this.modal.show();
    }

    hide(event) {
        this.modal.hide()
    }
}
