<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210525114331 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Booking entity';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE booking (
            id SERIAL,
            user_id INT NOT NULL,
            status_id INT DEFAULT NULL,
            date_start DATE DEFAULT NULL,
            date_end DATE DEFAULT NULL,
            number_of_people INT NOT NULL,
            date_created timestamp NOT NULL DEFAULT NOW(),
            PRIMARY KEY(id)
        )');
        $this->addSql('CREATE INDEX IDX_E00CEDDEA76ED395 ON booking (user_id)');
        $this->addSql('CREATE INDEX IDX_E00CEDDE6BF700BD ON booking (status_id)');
        $this->addSql('CREATE TABLE booking_status (id SERIAL, name VARCHAR(16) DEFAULT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDEA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE6BF700BD FOREIGN KEY (status_id) REFERENCES booking_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users ALTER email DROP NOT NULL');

        $query = <<<'SQL'
            INSERT INTO booking_status(name, description)
            VALUES
            ('CREATED', 'When the booking is created'),
            ('VALIDATED', 'When the booking is validated'),
            ('REFUSED', 'When the booking is refused'),
            ('CANCELED', 'When the booking is canceled by the user');
        SQL;

        $this->addSql($query);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE booking DROP CONSTRAINT FK_E00CEDDE6BF700BD');
        $this->addSql('DROP TABLE booking');
        $this->addSql('DROP TABLE booking_status');
        $this->addSql('ALTER TABLE users ALTER email SET NOT NULL');
    }
}
